<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Ticket Factory</title>

    <!-- JQUERY -->
    <script type="text/javascript" src="bootstrap/dist/jquery.js"></script> 

    <!-- CCS -->
    <link href="style.css" rel="stylesheet">

    <!-- BOOSTRAP -->
    <link href="bootstrap/dist/css/bootstrap.css" rel="stylesheet">
    <script type="text/javascript" src="bootstrap/dist/js/bootstrap.js"></script> 

	<link rel="icon" type="image/png" href="images/favicon.png" />
  </head>

  <body>

    <?php
    // GESTION DES SESSIONS, DECONNEXION AUTOMATIQUE APRES 1H
    session_start();
    ob_start();
    
    if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 120*60)) {
      header('Location: deconnexion.php');
    }
    $_SESSION['LAST_ACTIVITY'] = time();

    if(!isset($_SESSION["technicien"])) header("Location: index.php"); 
    date_default_timezone_set('Europe/Paris');
    include("functionBDD.php");
    include("modal.php");


    $requete = $bdd->prepare('DELETE FROM notification WHERE Id_ticket=:id AND Destinataire=:destinataire');
    $requete->bindParam(':id', $_GET['ticket']);
    $requete->bindParam(':destinataire', $_SESSION['technicien']);
    $requete->execute();
    $requete->closeCursor();
    

    if(isset($_GET['add']) && $_GET['add'] == "true"){
      if(isset($_POST['valider']) && $_POST['valider'] == "ajouter"){
        // AJOUT D'UNE INTERVENTION
        $requete = $bdd->prepare('INSERT INTO interventions VALUES(:id, :creation, :technicien, :intervention)');
        $requete->bindParam(':id', $_GET['ticket']);
        $date = date("Y-m-d H:i:s");
        $requete->bindParam(':creation', $date);
        $requete->bindParam(':technicien', $_SESSION['technicien']);
        $requete->bindParam(':intervention', $_POST['description']);
        $requete->execute();
        $requete = $bdd->prepare('SELECT * FROM tickets WHERE id=:id');
        $requete->bindParam(':id', $_GET['ticket']);
        $requete->execute();
        $donnees = $requete->fetch();
        // AJOUT DE LA NOTIFICATION POUR LE CREATEUR
        if($_SESSION['technicien'] != $donnees['createur']){
          $requete = $bdd->prepare('INSERT INTO notification VALUES(:id, :description, :destinataire)');
          $requete->bindParam(':id', $_GET['ticket']);
          $requete->bindParam(':destinataire', $donnees['createur']);
          $desc_notif = "Une intervention a été ajoutée par ".$_SESSION['technicien'];
          $requete->bindParam(':description', $desc_notif);
          $requete->execute();
        }
        // AJOUT DE LA NOTIFICATION POUR LE PROPRIETAIRE
        if($donnees['technicien']!="A définir" && $_SESSION['technicien'] != $donnees['technicien'] && $donnees['createur']!=$donnees['technicien']){
          $requete = $bdd->prepare('INSERT INTO notification VALUES(:id, :description, :destinataire)');
          $requete->bindParam(':id', $_GET['ticket']);
          $requete->bindParam(':destinataire', $donnees['technicien']);
          $desc_notif = "Une intervention a été ajoutée par ".$_SESSION['technicien'];
          $requete->bindParam(':description', $desc_notif);
          $requete->execute();
        }
        $requete->closeCursor();
      }

      if(isset($_GET['valider']) && $_GET['valider'] == "cloturer"){
        // CLOTURE DU TICKET 
        $requete = $bdd->prepare('SELECT technicien FROM tickets WHERE id = :id');
        $requete->bindParam(':id', $_GET['ticket']);
        $requete->execute();
        $donnees = $requete->fetch();
        if($donnees['technicien']=="A définir"){
          $requete = $bdd->prepare('UPDATE tickets SET technicien = "Ticket non assigné" WHERE id = :id');
          $requete->bindParam(':id', $_GET['ticket']);
          $requete->execute();
        }
        $requete = $bdd->prepare('INSERT INTO interventions VALUES(:id, :creation, :technicien, :intervention)');
        $requete->bindParam(':id', $_GET['ticket']);
        $date = date("Y-m-d H:i:s");
        $requete->bindParam(':creation', $date);
        $requete->bindParam(':technicien', $_SESSION['technicien']);
        $desc_cloture = "Ticket clôturé le ".date("d/m/Y H:i:s")." par ".$_SESSION['technicien'];
        $requete->bindParam(':intervention', $desc_cloture);
        $requete->execute();
        $requete = $bdd->prepare('UPDATE tickets SET importance = 0 WHERE id = :id');
        $requete->bindParam(':id', $_GET['ticket']);
        $requete->execute();
        $requete = $bdd->prepare('SELECT * FROM tickets WHERE id=:id');
        $requete->bindParam(':id', $_GET['ticket']);
        $requete->execute();
        $donnees = $requete->fetch();
        // AJOUT DE LA NOTIFICATION POUR LE CREATEUR
        if($_SESSION['technicien'] != $donnees['createur']){
          $requete = $bdd->prepare('INSERT INTO notification VALUES(:id, :description, :destinataire)');
          $requete->bindParam(':id', $_GET['ticket']);
          $requete->bindParam(':destinataire', $donnees['createur']);
          $desc_notif = "Le ticket a été clôturé par ".$_SESSION['technicien'];
          $requete->bindParam(':description', $desc_notif);
          $requete->execute();
        }
        // AJOUT DE LA NOTIFICATION POUR LE PROPRIETAIRE
        if($_SESSION['technicien'] != $donnees['technicien']  && $donnees['createur']!=$donnees['technicien']){
          $requete = $bdd->prepare('INSERT INTO notification VALUES(:id, :description, :destinataire)');
          $requete->bindParam(':id', $_GET['ticket']);
          $requete->bindParam(':destinataire', $donnees['technicien']);
          $desc_notif = "Le ticket a été clôturé par ".$_SESSION['technicien'];
          $requete->bindParam(':description', $desc_notif);
          $requete->execute();
        }

        $requete->closeCursor();
      }

      if(isset($_POST['valider']) && $_POST['valider'] == "modifier"){
        //MODIFICATION DU PROPRIETAIRE DU TICKET
        $requete = $bdd->prepare('UPDATE tickets SET technicien = :technicien WHERE id = :id');
        $requete->bindParam(':technicien', $_POST['owner']);
        $requete->bindParam(':id', $_GET['ticket']);
        $requete->execute();
        $requete = $bdd->prepare('INSERT INTO interventions VALUES(:id, :creation, :technicien, :intervention)');
        $requete->bindParam(':id', $_GET['ticket']);
        $date = date("Y-m-d H:i:s");
        $requete->bindParam(':creation', $date);
        $requete->bindParam(':technicien', $_SESSION['technicien']);
        $desc_modif = "Ticket affecté à ".$_POST['owner']." par ".$_SESSION['technicien'];
        $requete->bindParam(':intervention', $desc_modif);
        $requete->execute();
        $requete = $bdd->prepare('SELECT * FROM tickets WHERE id=:id');
        $requete->bindParam(':id', $_GET['ticket']);
        $requete->execute();
        $donnees = $requete->fetch();
        // AJOUT DE LA NOTIFICATION POUR LE CREATEUR
        if($_SESSION['technicien'] != $donnees['createur']){
          $requete = $bdd->prepare('INSERT INTO notification VALUES(:id, :description, :destinataire)');
          $requete->bindParam(':id', $_GET['ticket']);
          $requete->bindParam(':destinataire', $donnees['createur']);
          $desc_notif = "Le ticket a été assigné à ".$_POST['owner'];
          $requete->bindParam(':description', $desc_notif);
          $requete->execute();
        }
        // AJOUT DE LA NOTIFICATION POUR LE PROPRIETAIRE
        if($_SESSION['technicien'] != $_POST['owner']  && $donnees['createur']!=$donnees['technicien']){
          $requete = $bdd->prepare('INSERT INTO notification VALUES(:id, :description, :destinataire)');
          $requete->bindParam(':id', $_GET['ticket']);
          $requete->bindParam(':destinataire', $_POST['owner']);
          $desc_notif = "Le ticket a été assigné à ".$_POST['owner'];
          $requete->bindParam(':description', $desc_notif);
          $requete->execute();
        }
        $requete->closeCursor();
      } 
    }  
    ?>


    <!-- BANDEAU DE NAVIGATION -->
    <?php
    $_SESSION['currentPage'] = "detailTicket.php";
    include("bandeauNavigation.php")
    ?>

    <div class="container-fluid home">
      <div class="row">
        <div class="col-lg-1">
          <?php
            echo "<a href='".$_SESSION['lastPage']."'><button class='btn btn-dark' type='button'><img src='images/back.svg'><b> Retour</b></button></a>";
          ?>
        </div>
        <div class="col-lg-7 ticket">
          <?php

          $requete = $bdd->prepare('SELECT * FROM tickets WHERE id = :id');
          $requete->bindParam(':id', $_GET['ticket']);
          $requete->execute();
          $donnees = $requete->fetch();
          $client = $donnees['client'];
          $etat = $donnees['importance'];
          $owner = $donnees['technicien'];
          $createur = $donnees['createur'];
		
		      echo "<table style='width: 90%'><tr><td colspan=2 style='text-align: left'><p class='headerTicket'><b>Ticket n° ".$_GET['ticket']." :</b> ".$donnees['titre']."</p></td></tr>";
          echo "<tr><td style='text-align: left'><p class='headerTicket'><b>Créateur : </b>".$createur."</p></td>";
          echo "<td style='text-align: left'><p class='headerTicket'><b>Affecté à : </b>".$owner."</p></td></tr></table></br>";
          createCard($donnees['id'], $donnees['client'], $donnees['description'], $donnees['technicien'], $donnees['date'], $donnees['importance']);

          echo "<br/><h3>Interventions réalisées</h3><br/>";
          $requete = $bdd->prepare('SELECT * FROM interventions
 WHERE id = :id');
          $requete->bindParam(':id', $_GET['ticket']);
          $requete->execute();
          if($requete->rowCount() == 0) echo "<p>Aucune intervention pour ce ticket</p>";
          else{
            while($donnees = $requete->fetch()){
              createCardIntervention($donnees['intervention'], $donnees['date'], $donnees['technicien']);
            }
          }
          
          if($owner == "A définir"){
            createCardUser();
          }

          if($etat != 0){
            ?>
            <br/><h3>Ajouter une intervention </h3><br/>
            <?php echo "<form method='POST' class='form' action='detailTicket.php?ticket=".$_GET['ticket']."&add=true'>"; ?>
              <fieldset>
                <div class="form-row">
                  <div class="col-lg-12">
                    <textarea class="form-control" name="description" size="50" rows="5"></textarea>
                  </div>    
                </div>

                <div class="form-row">
                  <div class="col-lg-12">
                    <button class="btn btn-success" name="valider" value="ajouter" type="submit" style="margin-top: 2%">Ajouter</button>
                    <button class="btn btn-danger" name="valider" value="cloturer" type="button" style="margin-top: 2%; float: right" data-toggle='modal' data-target='#cloture'>Clôturer</button>
                  </div>
                </div>
              </fieldset>
            </form>
            <?php
          }
          ?>
        </div>

        <div class="col-lg-3">
          
          <h3 style="text-align: center" class="separateur">Informations sur le client</h3><br/>
          <?php
            ////// INFO client + REDIRECTION VERS SITE MV //////
            $requete = $bdd->prepare('SELECT id, nom FROM client WHERE nom = :nom');
            $requete->bindParam(':nom', $client);
            $requete->execute();
            $donnees = $requete->fetch();
            echo "<p style='text-align: center'><button type='button' class='btn btn-warning' style='font-size: 20pt'>".$client."</button></p>";

            ////// SECTION PIECE JOINTE //////
            echo "<h3 class='separateur'>Liste des piéces jointes relatives au ticket :</h3>";
            $path = 'PJ/'.$_GET['ticket'];
            
            // TEST DE L'EXISTENCE DU DOSSIER DE RECEPTION DES PIECE JOINTE
            if(is_dir($path)){
              $dossier = opendir('PJ/'.$_GET['ticket']);
              while(false !== ($fichier = readdir($dossier))){
                if($fichier != '.' && $fichier != '..' && $fichier != 'index.php'){
                  echo '<li><a href="'.$path.'/'.$fichier.'">'.$fichier .'</a></li>';
                }
              }
            }
            else{
              echo "<p style='margin-bottom:-5px'>Aucune pièce jointe pour ce client</p>";
            } 

            // UPLOAD D'UN FICHIER
            if(isset($_FILES['fichier'])){
              $fichier = basename($_FILES['fichier']['name']);
              $fichier = suppAccent($fichier);
              $taille_maxi = 10485760;
              $taille = filesize($_FILES['fichier']['tmp_name']);
              $extensions = array('.png', '.gif', '.jpg', '.jpeg','.txt','.pdf','.docx', '.PNG');
              $extension = strrchr($_FILES['fichier']['name'], '.'); 

              //Début des vérifications de sécurité...
              if(!in_array($extension, $extensions)){ //Si l'extension n'est pas dans le tableau
                $erreur = 'Vous devez uploader un fichier de type png, gif, jpg, jpeg, txt, pdf ou docx...';
              }
              if($taille>$taille_maxi){
                $erreur = 'Le fichier est trop gros...';
              }
              if(!isset($erreur)){
                if(is_dir($path)==false) mkdir($path);
                
                if(move_uploaded_file($_FILES['fichier']['tmp_name'], $path.'/'.$fichier)){ //Si la fonction renvoie TRUE, c'est que ça a fonctionné...
                  header('Location:detailTicket.php?ticket='.$_GET['ticket']);
                }
                else{ //Sinon (la fonction renvoie FALSE).
                  echo '<p>Echec de l\'upload !</p>';
                }
              }
              else{
                echo $erreur;
              }
            }
            ob_end_flush();
          ?>

          <form method="POST" enctype="multipart/form-data"><br/>

            <div class="custom-file">
              <input type="hidden" name="MAX_FILE_SIZE" value="10485760">
              <input type="file" class="custom-file-input" name="fichier" onchange="$('#display').html(this.files[0].name)" required>
              <label class="custom-file-label" for="fichier" id="display"></label>
            </div><br/><br/>
            <input type="submit" name="envoyer" value="Envoyer le fichier" class="btn btn-primary">
            
          </form>      
        </div>
      </div>
    </div>
    
  </body>
</html>

<?php

function createCard($id, $client, $description, $owner, $creation, $etat){
  sscanf($creation, "%4s-%2s-%2s %2s:%2s", $an, $mois, $jour, $heure, $minutes);
  $date = date_create($an."-".$mois."-".$jour." ".$heure.":".$minutes);

  $header = "Crée le ".date_format($date, 'd/m/Y H:i');

  if($etat == "1") echo "<div class='card bg-clear mb-3'>";
  else if($etat == "0") echo "<div class='card text-white bg-success mb-3'>";
  else if($etat == "2") echo "<div class='card text-white bg-danger mb-3'>";
    echo "<div class='card-header'>".$header."</div>";
    echo "<div class='card-body'>";
      echo "<p class='card-text'>".nl2br($description)."</p>";
    echo "</div>";
  echo "</div>";
}

function createCardIntervention($description, $edition, $editeur){
  sscanf($edition, "%4s-%2s-%2s %2s:%2s", $an, $mois, $jour, $heure, $minutes);
  $date = date_create($an."-".$mois."-".$jour." ".$heure.":".$minutes);

  $header = "Le ".date_format($date, 'd/m/Y H:i')." par ".$editeur;

  echo "<div class='card text-white bg-secondary mb-3'>";
    echo "<div class='card-header'>".$header."</div>";
    echo "<div class='card-body'>";
      echo "<p class='card-text'>".nl2br($description)."</p>";
    echo "</div>";
  echo "</div>";
}

function createCardUser(){
  include("functionBDD.php");
  echo "<div class='card text-white bg-secondary mb-3'>";
    echo "<div class='card-header' style='font-weight: bold;'>Veuillez définir un propriétaire</div>";
    echo "<div class='card-body'>";
      $requete = $bdd->query('SELECT * FROM users WHERE Actif=true');
      echo "<form method=POST action='detailTicket.php?ticket=".$_GET['ticket']."&add=true'>";
        echo "<select name=owner class='form-control'>";
          while($donnees = $requete->fetch()){
            echo "<option value='".$donnees['Pseudo']."'>".$donnees['Prenom']." ".$donnees['Nom']."</option>";
          }
        echo "</select></br>";
        echo "<button class='btn btn-primary' name='valider' value='modifier' type='submit' style='float: right'>Modifier</button>";
      echo "</form>";
    echo "</div>";
  echo "</div>";
}

function suppAccent($chaine){
    $a = array("ä", "â", "à");
    $chaine = str_replace($a, "a", $chaine);


    $e = array("é", "è", "ê", "ë");
    $chaine = str_replace($e, "e", $chaine);

    $c = array("ç");
    $chaine = str_replace($c, "c", $chaine);
     
    return $chaine;

}
?>