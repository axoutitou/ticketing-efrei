<?php
	session_start();

	// Destruction des variables de session utilisé dans mon site (et non globale) pour ne pas interférer avec ceux de Michael
	
	$varSession = array($_SESSION['technicien'], $_SESSION['admin'], $_SESSION['LAST_ACTIVITY']);

	foreach($varSession as $var) unset($var);

	header('location: index.php');
	exit;
?>