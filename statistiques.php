<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Ticket Factory</title>

    <!-- JQUERY -->
    <script type="text/javascript" src="bootstrap/dist/jquery.js"></script> 

    <!-- CCS -->
    <link href="style.css" rel="stylesheet">

    <!-- BOOSTRAP -->
    <link href="bootstrap/dist/css/bootstrap.css" rel="stylesheet">
    <script type="text/javascript" src="bootstrap/dist/js/bootstrap.js"></script>   

    <link rel="icon" type="image/png" href="images/favicon.png" />
  </head>

  <body>
  <!-- BANDEAU DE NAVIGATION -->
  <?php
  session_start();

  if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 120*60)) {
    header('Location: deconnexion.php');
  }
  $_SESSION['LAST_ACTIVITY'] = time();
  if(!isset($_SESSION["technicien"])) header("Location: index.php");
  
  include("functionBDD.php");

  $_SESSION['currentPage'] = "statistiques.php";
  include("bandeauNavigation.php");
  
  // GENERATION DES DIAGRAMMES
  //Récupération des données
  $requete = $bdd->query('SELECT technicien, count(*) FROM tickets  WHERE importance="0" GROUP BY technicien');
  $nb = $requete->fetchAll(PDO::FETCH_COLUMN, 1);
  $requete = $bdd->query('SELECT technicien, count(*) FROM tickets WHERE importance="0" GROUP BY technicien');
  $user = $requete->fetchAll(PDO::FETCH_COLUMN, 0);

  // Standard inclusions     
  include("pChart/pData.class");  
  include("pChart/pChart.class");  
    
  // Dataset definition   
  $DataSet = new pData;  
  $DataSet->AddPoint($nb,"Serie1");  
  $DataSet->AddPoint($user,"Serie2");  
  $DataSet->AddAllSeries();  
  $DataSet->SetAbsciseLabelSerie("Serie2");  
    
  // Initialise the graph  
  $Test = new pChart(300,200);  
  $Test->loadColorPalette("Sample/softtones.txt");  
  $Test->drawFilledRoundedRectangle(7,7,293,193,5,240,240,240);  
  $Test->drawRoundedRectangle(5,5,295,195,5,230,230,230);  
    
  // This will draw a shadow under the pie chart  
  $Test->drawFilledCircle(122,102,70,200,200,200);  
    
  // Draw the pie chart  
  $Test->setFontProperties("Fonts/tahoma.ttf",8);  
  $Test->drawBasicPieGraph($DataSet->GetData(),$DataSet->GetDataDescription(),120,100,70,PIE_PERCENTAGE,255,255,218);  
  $Test->drawPieLegend(210,15,$DataSet->GetData(),$DataSet->GetDataDescription(),250,250,250);  
    
  $Test->Render("Generated/ticektPeruser.png"); 
  ?>

  <div class="container-fluid home">
    <div class="row">
      <div class="offset-lg-1 col-lg-10">
        <h1>Nombre de ticket cloturé par technicien</h1>
      </div>         
    </div>

    <div class="row">
      <div class="col-lg-12 text-center">
        <img src='Generated/ticektPeruser.png' style='padding-top: 50px;' width='35%'>
      </div>
    </div>
  </div>

  
  </body>
</html>