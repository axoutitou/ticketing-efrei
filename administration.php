<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Ticket Factory</title>

    <!-- JQUERY -->
    <script type="text/javascript" src="bootstrap/dist/jquery.js"></script> 
    <script type="text/javascript" src="DataTables/media/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="DataTables/media/js/dataTables.jqueryui.min.js"></script>

    <!-- CCS -->
    <link href="style.css" rel="stylesheet">

    <!-- BOOSTRAP -->
    <link href="bootstrap/dist/css/bootstrap.css" rel="stylesheet">
    <script type="text/javascript" src="bootstrap/dist/js/bootstrap.js"></script>   
    
    <!-- DATATABLES -->
    <link rel="stylesheet" type="text/css" href="DataTables/media/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="DataTables/media/css/dataTables.jqueryui.min.css">
    <link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <link rel="icon" type="image/png" href="images/favicon.png" />
  </head>

  <body>

  <!-- BANDEAU DE NAVIGATION -->
  <?php
  session_start();

  if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 120*60)) {
    header('Location: deconnexion.php');
  }
  $_SESSION['LAST_ACTIVITY'] = time();
  if(!isset($_SESSION["technicien"])) header("Location: index.php");

  include("functionBDD.php");
  include("modal.php");

  $_SESSION['currentPage'] = "administration.php";
  include("bandeauNavigation.php");
  ?>    

  <!-- PARAMETRE DE LA DATATABLE -->
  <script>
    $(document).ready(function () {
      $('#utilisateurs').DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
        },
        "columns": [
          { "width": "50px" },
          null,
          null,
          null,
          { "width": "700px" }
        ],
        "scrollY":        ($(window).height() - 450),
        "scrollCollapse": true,
        "paging":         false,
        "info":           false,
        "ordering":       false,
      });
    });

    function validation(){
      $(document).ready(function(){
          $("#validation").modal();
      });
    }

    function modification(){
      $(document).ready(function(){
          $("#motDePasse").modal();
      });
    }    

    function desactivation(){
      $(document).ready(function(){
          $("#desactivation").modal();
      });
    } 

    function activation(){
      $(document).ready(function(){
          $("#activation").modal();
      });
    }   
  </script>

  <?php
  if(isset($_POST['ajout']) && $_POST['ajout'] == "Ajouter"){
    $requete = $bdd->prepare("INSERT INTO users(`Nom`, `Prenom`, `Pseudo`, `Admin`) VALUES (:nom, :prenom, :pseudo, :administrateur)");
    $nomUpper = strtoupper($_POST["nom"]);
    $requete->bindParam(":nom", $nomUpper);
    $requete->bindParam(":prenom",$_POST["prenom"]);
    $pseudo = strtolower(substr($_POST["prenom"], 0, 1).$_POST['nom']);
    $requete->bindParam(":pseudo",$pseudo);
    if(isset($_POST["admin"])) $admin = 1;
    else $admin = 0;
    $requete->bindParam(":administrateur",$admin);
    $requete->execute();
    echo "<script>validation()</script>";
  }

  if(isset($_POST['reinitialiser'])){
    $requete = $bdd->prepare("UPDATE users SET Mdp='5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8' WHERE Id = :id");
    $requete->bindParam(":id", $_POST['reinitialiser']);
    $requete->execute();
    echo "<script>modification()</script>";
  }
  
  if(isset($_POST['desactiver'])){
    $requete = $bdd->prepare("UPDATE users SET Actif=false WHERE ID = :id");
    $requete->bindParam(":id", $_POST['desactiver']);
    $requete->execute();
    echo "<script>desactivation()</script>";
  }

  if(isset($_POST['activer'])){
    $requete = $bdd->prepare("UPDATE users SET Actif=true WHERE ID = :id");
    $requete->bindParam(":id", $_POST['activer']);
    $requete->execute();
    echo "<script>activation()</script>";
  }

  if(isset($_POST['upgrade'])){
    $requete = $bdd->prepare("UPDATE users SET Admin=1 WHERE ID = :id");
    $requete->bindParam(":id", $_POST['upgrade']);
    $requete->execute();
    echo "<script>modification()</script>";
  }

  ?>

  <div class="container-fluid">
    <!-- DATATABLE -->
    <div class="row">
      <div class="col-lg-12">
        <br/><legend>Liste des utilisateurs</legend>
        <table id='utilisateurs' class='display'>
          <thead>
            <tr><th>ID</th><th>Nom</th><th>Prenom</th><th>Login</th><th>Actions</th></tr>
          </thead>
          <tbody>
            <?php
              $requete = $bdd->query("SELECT * FROM users");
              while($donnees = $requete->fetch()){
                ajouteLigne($donnees['Id'], $donnees['Nom'], $donnees['Prenom'], $donnees['Pseudo'], $donnees['Admin'], $donnees['Actif']);
              }
            ?>
          </tbody>
        </table><br/>
        

        <form method="post" class="form-inline" style="margin-bottom: 20px;" id=newUser>
          <legend>Création d'un nouvel utilisateur</legend>
                   
          <input type="text" class="form-control" name="nom" required="required" placeholder="Nom" autocomplete="off">     
          <input type="text" class="form-control" name="prenom" required="required" placeholder="Prenom" autocomplete="off">
        
          <input type="checkbox" name="admin" id="admin" value="1">
          <label for="admin">Administrateur </label>    
        
          <button type="submit" value="Ajouter" name="ajout" class="btn btn-success">Ajouter</button>
        </form>
      </div>  
    </div>
  </div>
  
  <?php
    function ajouteLigne($id, $nom, $prenom, $login,$admin, $actif){
      if($admin == "1") $buttonUp = "<button class='btn btn-info' name='upgrade' type='submit' value='".$id."' disabled>Monter administrateur</button>";
      else $buttonUp = "<button class='btn btn-info' name='upgrade' type='submit' value='".$id."'>Monter administrateur</button>";

      if($actif) $buttonGest =  "<button class='btn btn-danger' name='desactiver' type='submit' value='".$id."'>Desactiver</button> <button class='btn btn-success' name='activer' type='submit' value='".$id."' disabled>Activer</button>";
      else $buttonGest =  "<button class='btn btn-danger' name='desactiver' type='submit' value='".$id."' disabled>Desactiver</button> <button class='btn btn-success' name='activer' type='submit' value='".$id."'>Activer</button>";
      
      echo "<tr>
        <td>".$id."</td>
        <td>".$nom."</td>
        <td>".$prenom."</td>
        <td>".$login."</td>
        <td>
          <form method='POST'>
            <button class='btn btn-secondary' name='reinitialiser' type='submit' value='".$id."'>Réinitialiser le mot de passe</button>
            ".$buttonGest."
            ".$buttonUp."
          </form>    
        </td>
      </tr>";
    }
  ?>
  </body>
</html>