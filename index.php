<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Ticket Factory</title>

    <!-- CCS -->
    <link href="bootstrap/dist/css/bootstrap.css" rel="stylesheet">
    <link href="style.css" rel="stylesheet">

	<link rel="icon" type="image/png" href="images/favicon.png" />
	
  </head>

  <body class="body-login">
    <?php
    include("functionBDD.php");
    ?>

    <div class="container">

      <div class="row">
        <div class="offset-lg-4 col-lg-4" style="text-align: center">

          <!-- Formulaire de connexion -->
          <form method="POST" class="login-form">
            <h3>Bienvenue !</h3>
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Identifiant" name="login" id="login"/>
            </div>
            <div class="form-group">
              <input type="password" class="form-control" placeholder="Mot de passe" name="mdp" id="mdp" />
            </div>
            <div class="form-group">
             <input type="submit" class="btnSubmit" value="Login" id ="submit"/>
            </div>
          </form>

          <p id="erreur"></p>
        </div>
      </div>

    </div>

    <script language="javascript">
    function erreur(){
      document.getElementById("login").className="form-control is-invalid",
      document.getElementById("mdp").className="form-control is-invalid";
      document.getElementById("erreur").innerHTML="Veuillez vérifier vos identifiants";
    }
  </script>

    <?php

    if(isset($_POST['login']) && isset($_POST['mdp'])){
      $login=$_POST['login'];
      $mdp=$_POST['mdp'];

      $requete = $bdd->query('SELECT * FROM users WHERE Actif=true');

      while ($donnees = $requete->fetch()){
        if($donnees['Pseudo'] == $login && $donnees['Mdp'] == sha1($mdp)){
          session_start();
          $_SESSION['technicien'] = $login;
          $_SESSION['admin'] = $donnees['Admin'];
          if($donnees['Mdp'] == "5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8") header('Location: home.php?filtre=current&first=true');
          else header('Location: home.php?filtre=current');
        }
      }
      echo "<script language='javascript'> erreur(); </script>";  
    }
    ?>
  </body>
</html>