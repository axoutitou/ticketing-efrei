<!-- GESTION DES NOTIFICATIONS -->
  <div style="margin-right: 100px; margin-top: -30px">
    <div class="dropdown dropleft" style="position: absolute;z-index: 1">
        <img src="images/notification.png" class="dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="cursor:pointer;"/>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
          <?php
          $requete = $bdd->prepare('SELECT * FROM notification n JOIN tickets t ON(n.Id_ticket=t.id) WHERE Destinataire=:technicien');
          $requete->bindParam(':technicien', $_SESSION['technicien']);
          $requete->execute();
          $nb = 0;
          while( $donnees = $requete->fetch() ){
            if($nb == 0) echo "<a class='dropdown-item' href='detailTicket.php?ticket=".$donnees['id']."'><strong>Ticket n°".$donnees['id']." : ".$donnees['client']."</strong><br/>".$donnees['Description']."</a>";
            else echo "<a class='dropdown-item style' href='detailTicket.php?ticket=".$donnees['id']."'><strong>Ticket n°".$donnees['id']." : ".$donnees['client']."</strong><br/>".$donnees['Description']."</a>";

            $nb = $nb+1;
          }
          if($nb == 0) echo "<a class='dropdown-item'> Aucune notification </a>";
          $requete->closeCursor();
          ?>
        </div>
    </div>
    <div style="position: absolute;z-index: 2; right: 200px; top:20px;">
      <?php

      if($nb>0) echo "<p class=notification>".$nb."</p>";
      $requete->closeCursor();

      ?>
    </div>
  </div>