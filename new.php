<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Ticket Factory</title>

    <!-- JQUERY -->
    <script type="text/javascript" src="bootstrap/dist/jquery.js"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>   

    <!-- CCS -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <link href="style.css" rel="stylesheet">  

    <!-- BOOSTRAP -->
    <link href="bootstrap/dist/css/bootstrap.css" rel="stylesheet">
    <script type="text/javascript" src="bootstrap/dist/js/bootstrap.js"></script>

    <link rel="icon" type="image/png" href="images/favicon.png" />
  </head>

  <body>


    <script>
    var availableTags = [];

    function validation(){
      $(document).ready(function(){
          $("#validation").modal();
      });
    }

    $( function() {
      $( "#nom" ).autocomplete({
        source: availableTags
      });

    } );
    </script>

    <?php
    // GESTION DES SESSIONS, DECONNEXION AUTOMATIQUE APRES 1H
    session_start();
    
    if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 120*60)) {
      header('Location: deconnexion.php');
    }
    $_SESSION['LAST_ACTIVITY'] = time();
    if(!isset($_SESSION["technicien"])) header("Location: index.php");

    include("functionBDD.php");
    include("modal.php");

    if(isset($_POST['valider'])){
      if($_POST['valider'] == 'ticket'){
        $requete = $bdd->prepare('INSERT INTO tickets VALUES (null, :client, :description, :titre, :importance, :creation, :createur, :technicien, "", null)');
        $date = date("Y-m-d H:i:s");
        $requete->bindParam(':creation', $date);
        $requete->bindParam(':createur', $_SESSION['technicien']);
      }
      else if ($_POST['valider'] == 'modifier'){
        $requete = $bdd->prepare('UPDATE tickets SET client=:client, importance=:importance, titre=:titre, description=:description, technicien=:technicien WHERE id=:id');
        $requete->bindParam(':id', $_GET['ticket']);
        unset($_GET['ticket']);
      }
      $requete->bindParam(':client', $_POST['nom']);
      $requete->bindParam(':description', $_POST['description']);
      $requete->bindParam(':titre', $_POST['titre']);
      $requete->bindParam(':importance', $_POST['importance']);
      $requete->bindParam(':technicien', $_POST['technicien']);
      $requete->execute();
      echo "<script>validation()</script>";
        
    }

    if(isset($_GET["ticket"])){
      $requete = $bdd->prepare('SELECT * FROM tickets WHERE id=:id');
      $requete->execute(array(
        "id" => $_GET['ticket']
      ));
      $donnees = $requete->fetch();
      $client = $donnees['client'];
      $importance = $donnees['importance'];
      $technicien = $donnees['technicien'];
      $titre = $donnees['titre'];
      $description = $donnees['description'];
    }

    if(isset($_POST['valider']) && $_POST['valider']=="cds"){
      $requete = $bdd->prepare('INSERT INTO client (nom) VALUES (:nom)');
      $requete->execute(array(
        "nom" => $_POST['nom'],
        ));
      $requete->closeCursor();
      
      echo "<script>validation()</script>";
    }

    ?>

    <!-- BANDEAU DE NAVIGATION -->
    <?php
    $_SESSION['currentPage'] = "new.php";
    include("bandeauNavigation.php")
    ?>

    <!-- CREATION D'UN NOUVEAU TICKET -->
    <div class="container-fluid home">
      <div class="row">
        <div class="col-lg-6">
          <form method="post">
            
            <?php
            if(isset($_GET['ticket'])) echo "<legend>Modifier mon ticket </legend>";
            else echo "<legend>Créer un nouveau ticket </legend>";
        
            $requete = $bdd->query('SELECT nom FROM client ORDER BY nom');
            while ($donnees = $requete->fetch()){
              echo '<script> availableTags.push("'.$donnees['nom'].'") </script>';
            }
            $requete->closeCursor();
            ?>
            <div class="form-row">
              <div class="col-lg-12">
                <label for="nom">Nom du Client : </label>
                <?php
                  if(isset($client)) echo "<input name='nom' id='nom' class='form-control' value='".$client."' required><br/>";
                  else echo "<input name='nom' id='nom' class='form-control' required><br/>";                
                ?>
              </div>
            </div>
            

            <div class="form-row">
              <div class="form-group col-lg-6">
                <label for="importance">Importance :</label><br/>
                <div class="form-check">
                  <?php
                    echo "<input class='form-check-input' type='radio' name='importance'  value='1' id='normal' checked>";
                  ?>
                  <label class="form-check-label" for="normal">
                    Normal
                  </label>
                  
                </div>
                <div class="form-check">
                  <?php
                  if(isset($importance) && $importance==2) echo "<input class='form-check-input' type='radio' name='importance'  value='2' id='haute' checked>";
                  else echo "<input class='form-check-input' type='radio' name='importance'  value='2' id='haute'>";
                  ?>
                  <label class="form-check-label" for="haute">
                    Haute
                  </label>
                </div>
              </div>

              <div class="form-group col-lg-6">
                <label for="technicien">Pris en charge par : </label>
                <select name="technicien" id="technicien" class="form-control">
                  <?php $requete = $bdd->query('SELECT * FROM users WHERE Actif=true');
                  if(isset($technicien)){
                    while($donnees = $requete->fetch()){
                      if($donnees['Pseudo']==$technicien) echo "<option value='".$donnees['Pseudo']."' selected>".$donnees['Nom']." ".$donnees['Prenom']."</option>";
                      else echo "<option value='".$donnees['Pseudo']."'>".$donnees['Nom']." ".$donnees['Prenom']."</option>";
                    }
                  }                        
                  else{
                    while($donnees = $requete->fetch()){
                      if($donnees['Pseudo']==$_SESSION['technicien']) echo "<option value='".$donnees['Pseudo']."' selected>".$donnees['Nom']." ".$donnees['Prenom']."</option>";
                      else echo "<option value='".$donnees['Pseudo']."'>".$donnees['Nom']." ".$donnees['Prenom']."</option>";
                    }
                  }
                  
                  echo "<option value='A définir'>A définir</option>";
                  $requete->closeCursor();
                  ?>
                </select>
              </div>
            </div>

            <div class="form-row">
              <div class="col-lg-12">
                <label for="titre">Titre : </label><br/>
                <?php
                if(isset($titre)) echo "<input type='text' class='form-control' id='titre' name='titre' autocomplete='off' value='".$titre."'required>";
                else echo "<input type='text' class='form-control' id='titre' name='titre' autocomplete='off' required>";
                ?>
              </div>
            </div>

            <div class="form-row">
              <div class="col-lg-12">
                <label for="description">Description : </label><br/>
                <?php
                if(isset($titre)) echo "<textarea class='form-control' id='description' size='50' rows='10' name='description' required>".$description."</textarea>";
                else echo "<textarea class='form-control' id='description' size='50' rows='10' name='description' required></textarea>";
                ?>
              </div>
            </div>

            <div class="form-row">
              <div class="col-lg-2">
                <?php
                if(isset($_GET['ticket'])) echo "<button class='btn btn-success' type='submit' style='margin-top: 10%' name='valider' value='modifier'>Modifier</button>";
                else echo "<button class='btn btn-success' type='submit' style='margin-top: 10%' name='valider' value='ticket'>Créer</button>";
                ?>
              </div>
            </div>
          </form>
        </div>
        
        <!-- CREATION D'UN NOUVEAU CLIENT -->
        <div class="col-lg-6">
          <form method="POST" class="form">
            <fieldset>

                <legend>Création d'un nouveau Client</legend>
                <div class="form-row">
                  <div class="col-lg-12">
                    <label for="nom">Nom du Client : </label><br/>
                    <input type="text" class="form-control" name="nom" autocomplete="off" required>
                  </div>
                </div>

                <div class="form-row">
                  <div class="col-lg-2">
                    <button class="btn btn-success" type="submit" style="margin-top: 10%" name="valider" value="cds">Créer</button>
                  </div>
                </div>
              </fieldset>
            </form>
        </div>
      </div>
    </div>

  </body>
</html>

