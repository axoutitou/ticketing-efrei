<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Ticket Factory</title>

    <!-- JQUERY -->
    <script type="text/javascript" src="bootstrap/dist/jquery.js"></script> 

    <!-- CCS -->
    <link href="style.css" rel="stylesheet">

    <!-- BOOSTRAP -->
    <link href="bootstrap/dist/css/bootstrap.css" rel="stylesheet">
    <script type="text/javascript" src="bootstrap/dist/js/bootstrap.js"></script>   

    <link rel="icon" type="image/png" href="images/favicon.png" />
  </head>

  <body>
  <!-- BANDEAU DE NAVIGATION -->
  <?php
  session_start();

  if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 120*60)) {
    header('Location: deconnexion.php');
  }
  $_SESSION['LAST_ACTIVITY'] = time();
  if(!isset($_SESSION["technicien"])) header("Location: index.php");
  
  // include("functionBDD.php");
  // include("modal.php");

  $_SESSION['currentPage'] = "##.php";
  include("bandeauNavigation.php");
  ?>  

  </body>
</html>