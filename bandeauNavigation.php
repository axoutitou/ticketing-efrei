<nav class="navbar navbar-expand navbar-expand-md navbar-dark bg-primary">
  <a class="navbar-brand" href="home.php?filtre=current"><img src="images/logo.png" id="logo"/></a>
  
  <div class="collapse navbar-collapse" id="navbarText">

  <ul class="navbar-nav mr-auto">
    <li class="nav-item">
      <?php
      if($_SESSION['currentPage'] == "home.php") echo "<a href='home.php?filtre=current'><button class='btn btn-dark' type='button' disabled='disabled'>Gestion des tickets</button></a>";
      else echo "<a href='home.php?filtre=current'><button class='btn btn-dark' type='button'>Gestion des tickets</button></a>";
      ?>
    </li>

    <li class="nav-item">
      <?php
      if($_SESSION['currentPage'] == "new.php") echo "<a href='new.php'><button class='btn btn-dark' type='button' disabled='disabled'>Créer un ticket/CDS</button></a>";
      else echo "<a href='new.php'><button class='btn btn-dark' type='button' >Créer un ticket/CDS</button></a>";
      ?>
    </li>

    <?php if($_SESSION['admin'] == 1){ ?>
    <li class="nav-item">
      <?php
      if($_SESSION['currentPage'] == "administration.php") echo "<a href='administration.php'><button class='btn btn-dark' type='button' disabled='disabled'>Administration des utilisateurs</button></a>";
      else echo "<a href='administration.php'><button class='btn btn-dark' type='button'>Administration des utilisateurs</button></a>";
      ?> 
    </li>

    <li class="nav-item">
      <?php
      if($_SESSION['currentPage'] == "statistiques.php") echo "<a href='statistiques.php'><button class='btn btn-dark' type='button' disabled='disabled'>Statistiques</button></a>";
      else echo "<a href='statistiques.php'><button class='btn btn-dark' type='button'>Statistiques</button></a>";
      ?> 
    </li>

    <?php } ?>
  </ul>
  
  <form class="form-inline">

    <?php include("notification.php")?>

    <!-- DECONNEXION -->
    <a href="deconnexion.php"><button class="btn btn-outline-dark" id="deconnexion" type="button">Déconnexion</button></a>
  </form>
  
  </div>
</nav>