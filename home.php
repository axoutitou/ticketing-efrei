<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Ticket Factory</title>

    <!-- JQUERY -->
    <script type="text/javascript" src="bootstrap/dist/jquery.js"></script>
    <script type="text/javascript" src="DataTables/media/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="DataTables/media/js/dataTables.jqueryui.min.js"></script>
    <!-- CCS -->
    <link href="style.css" rel="stylesheet">
    <!-- BOOSTRAP -->
    <link href="bootstrap/dist/css/bootstrap.css" rel="stylesheet">
    <script type="text/javascript" src="bootstrap/dist/js/bootstrap.js"></script> 
    <!-- DATATABLES -->
    <link rel="stylesheet" type="text/css" href="DataTables/media/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="DataTables/media/css/dataTables.jqueryui.min.css">
    <link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    
	<link rel="icon" type="image/png" href="images/favicon.png" />
  </head>

  <body>

    <?php
    // GESTION DES SESSIONS, DECONNEXION AUTOMATIQUE APRES 1H
    session_start();
    
    if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 120*60)) {
      header('Location: deconnexion.php');
    }
    $_SESSION['LAST_ACTIVITY'] = time();
    if(!isset($_SESSION["technicien"])) header("Location: index.php");

    include("functionBDD.php");
    include("modal.php");
    ?>

    <!-- PARAMETRE DE LA DATATABLE -->
    <script>
    $(document).ready(function () {
      $('#ticket').DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
        },
        "columns": [
          null,
          null,
          { "width": "180px" },
          { "width": "180px" },
          null,
          null,
          null
        ],
        "scrollY":        ($(window).height() - 350),
        "scrollCollapse": true,
        "paging":         false,
        "info":           false,
        "ordering":       false,
      });
    });

    function FrontImage (){ 
       $("#btn-message").fadeTo(400, 0.25).fadeTo(400, 1);

    } 
    $(document).ready(function(){ 
        setInterval('FrontImage()',1000); 
    });

    function nouveauMDP(){
      $(document).ready(function(){
          $("#motDePasseNew").modal();
      });
    }
    </script>

    <!-- MODIFICATION DU MOT DE PASSE A LA PREMIERE CONNEXION -->
    <?php
    if(isset($_GET['first']) && $_GET['first']=='true'){
      echo "<script>nouveauMDP()</script>";
    }

    if(isset($_POST['valider']) && $_POST['valider']=="Modifier"){
        $requete = $bdd->prepare('UPDATE users SET Mdp= :mdp WHERE Pseudo=:pseudo');
        $pwdChiffre = sha1($_POST['password']);
        $requete->bindParam(':mdp', $pwdChiffre);
        $requete->bindParam(':pseudo', $_SESSION['technicien']);
        $requete->execute();
        $requete->closeCursor();
    }
    ?>

    <!-- BANDEAU DE NAVIGATION -->
    <?php
    $_SESSION['currentPage'] = "home.php";
    include("bandeauNavigation.php");
    ?>

    <div class="container-fluid home">

      <!-- FILTRES TICKETS -->
      <div class="row justify-content-center">
          <ul class="liste-filtre">
            <?php
            if(isset($_GET["filtre"])){

              if($_GET["filtre"] == "current"){
                echo "<li class='filtre'><a href='home.php?filtre=current'><button type='button' class='btn btn-success' disabled='disabled'>Tickets en cours</button></a></li>";
                $_SESSION['lastPage'] = "home.php?filtre=current";
              }
              else echo "<li class='filtre'><a href='home.php?filtre=current'><button type='button' class='btn btn-primary'>Tickets en cours</button></a></li>";

              if($_GET["filtre"] == "closed"){
                echo "<li class='filtre'><a href='home.php?filtre=closed'><button type='button' class='btn btn-success' disabled='disabled'>Tickets clos</button></a></li>";
                $_SESSION['lastPage'] = "home.php?filtre=closed";
              }
              else echo "<li class='filtre'><a href='home.php?filtre=closed'><button type='button' class='btn btn-primary'>Tickets clos</button></a></li>";

              if($_GET["filtre"] == "mine"){
                echo "<li class='filtre'><a href='home.php?filtre=mine'><button type='button' class='btn btn-success' disabled='disabled'>Mes tickets</button></a></li>";
                $_SESSION['lastPage'] = "home.php?filtre=mine";
              } 
              else echo "<li class='filtre'><a href='home.php?filtre=mine'><button type='button' class='btn btn-primary'>Mes tickets</button></a></li>";

              if($_GET["filtre"] == "all"){
                echo "<li class='filtre'><a href='home.php?filtre=all'><button type='button' class='btn btn-success' disabled='disabled'>Tous les tickets</button></a></li>";
                $_SESSION['lastPage'] = "home.php?filtre=all";
              } 
              else echo "<li class='filtre'><a href='home.php?filtre=all'><button type='button' class='btn btn-primary'>Tous les tickets</button></a></li>";
            }
            ?>
          </ul>
      </div>

      <?php
      $requete = $bdd->query('SELECT count(technicien) from tickets where technicien="A définir"');
      $donnees = $requete->fetch();

      if($donnees['count(technicien)'] != 0){
        echo "<div class='row justify-content-center'>";
            echo "<button type='button 'class='btn btn-danger' id='btn-message' style='margin-bottom: 10px;' data-toggle='modal' data-target='#message'><img src='images/alert-octagon.svg' style='margin-right:10px'/>TICKETS A DEFINIR   <img src='images/alert-octagon.svg' style='margin-left:10px'/></button>";
        echo "</div>";        
      }

      ?>

      <!-- DATATABLE -->
      <div class="row">
        <div class="col-lg-12">
          <table id='ticket' class='display'>

            <thead>
                <tr><th>ID</th><th>Etat</th><th>Client</th><th>Titre</th><th>Description</th><th>Date</th><th>Affectation</th></tr>
            </thead>
            <tbody>
              <?php


              if(isset($_GET["filtre"])){

                switch ($_GET["filtre"]) {
                  case "current":
                    $requete_userVide = $bdd->query('SELECT * FROM tickets WHERE technicien="A définir" ORDER BY id DESC');
                    $requete_important = $bdd->query('SELECT * FROM tickets WHERE importance=2 AND technicien!="A définir" ORDER BY id DESC');
                    $requete = $bdd->query('SELECT * FROM tickets WHERE importance=1 AND technicien!="A définir" ORDER BY id DESC');

                    while ($donnees = $requete_userVide->fetch() OR $donnees = $requete_important->fetch() OR $donnees = $requete->fetch()){
                      ajouteLigne($donnees['id'], $donnees['client'], $donnees['titre'], $donnees['description'], $donnees['date'], $donnees['technicien'], $donnees['importance'], $donnees['createur']);
                    }

                    $requete_userVide->closeCursor();
                    $requete_important->closeCursor();
                    $requete->closeCursor();
                    break;

                  case "closed":
                    $requete = $bdd->query('SELECT * FROM tickets WHERE importance=0 ORDER BY id DESC');

                    while ($donnees = $requete->fetch()){
                      ajouteLigne($donnees['id'], $donnees['client'], $donnees['titre'], $donnees['description'], $donnees['date'], $donnees['technicien'], $donnees['importance'], $donnees['createur']);
                    }

                    $requete->closeCursor();
                    break;

                  case "mine":
                    $requete_important = $bdd->prepare('SELECT * FROM tickets WHERE importance=2 AND technicien=:technicien ORDER BY id DESC');
                    $requete_important->bindParam(':technicien', $_SESSION['technicien']);
                    $requete_important->execute();
                    $requete = $bdd->prepare('SELECT * FROM tickets WHERE importance=1 and technicien=:technicien ORDER BY id DESC');
                    $requete->bindParam(':technicien', $_SESSION['technicien']);
                    $requete->execute();

                    while ($donnees = $requete_important->fetch() OR $donnees = $requete->fetch()){
                      ajouteLigne($donnees['id'], $donnees['client'], $donnees['titre'], $donnees['description'], $donnees['date'], $donnees['technicien'], $donnees['importance'], $donnees['createur']);
                    }

                    $requete_important->closeCursor();
                    $requete->closeCursor();

                    break;
                  case "all":
                    $requete_userVide = $bdd->query('SELECT * FROM tickets WHERE technicien="A définir" ORDER BY id DESC');
                    $requete_important = $bdd->query('SELECT * FROM tickets WHERE importance=2 AND technicien!="A définir" ORDER BY id DESC');
                    $requete = $bdd->query('SELECT * FROM tickets WHERE (importance=1 OR importance=0) AND technicien!="A définir" ORDER BY id DESC');

                    while ($donnees = $requete_userVide->fetch() OR $donnees = $requete_important->fetch() OR $donnees = $requete->fetch() ){
                      ajouteLigne($donnees['id'], $donnees['client'], $donnees['titre'], $donnees['description'], $donnees['date'], $donnees['technicien'], $donnees['importance'], $donnees['createur']);
                    }

                    $requete_userVide->closeCursor();
                    $requete_important->closeCursor();
                    $requete->closeCursor();

                    break;
                }
              }

              ?>
              </tbody>
          </table>
        </div>
      </div>
    </div>

    <?php
    function ajouteLigne($id, $client, $titre, $descriptionParam, $dateParam, $technicien, $importance, $owner){
      $taille = 240;
      $description = "";
      if(strlen($descriptionParam)>$taille) $description = substr($descriptionParam, 0, $taille)."[...]";
      else $description =  $descriptionParam;
      $descriptionFinal = str_replace("<br />"," ", $description);
      sscanf($dateParam, "%4s-%2s-%2s %2s:%2s", $an, $mois, $jour, $heure, $minutes);
      $date = date_create($an."-".$mois."-".$jour." ".$heure.":".$minutes);

      $etat = "";

      if($technicien == "A définir") $etat="<img src='images/user-x.svg'/>";
      else{
        if($importance == 0) $etat="<img src='images/check.svg'/>";
        else  if($importance == 1) $etat="<img src='images/clock.svg'/>";
        else  if($importance == 2) $etat="<img src='images/alert.svg'/>";
      }

      if($_SESSION['technicien']==$owner){
        echo "<tr><td>".$id."</td><td>".$etat."</td><td>".$client."</td><td>".$titre."</td><td style='text-align: left; padding-left: 40px;'>".$descriptionFinal."<a href='detailTicket.php?ticket=".$id."'><img src='images/plus.svg' style='float: right'></a><a href='new.php?ticket=".$id."'><img src='images/support.svg'/ style='float: right'></a></td><td>".$date->format("d/m/Y H:i:s")."</td><td>".$technicien."</td></tr>";
      }
      else{
        echo "<tr><td>".$id."</td><td>".$etat."</td><td>".$client."</td><td>".$titre."</td><td style='text-align: left; padding-left: 40px;'>".$descriptionFinal."<a href='detailTicket.php?ticket=".$id."'><img src='images/plus.svg' style='float: right'></a></td><td>".$date->format("d/m/Y H:i:s")."</td><td>".$technicien."</td></tr>";
      }
    }

    ?>
  </body>
</html>