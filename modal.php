<?php include("functionBDD.php") ?>

<!--AJOUT VALIDER -->
<div class="modal fade" id="validation" role="dialog">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalTitle" style="font-size: 16pt; font font-weight: bold;">Informations</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p style="font-size: 16pt; text-align: center">Ajout effectué avec succès !</p>
        <img src="images/checked.svg" style="width: 10%; margin: 0 40%;">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
      </div>
    </div>
  </div>
</div>

<!-- MODIFICATION REUSSI -->
<div class="modal fade" id="modification" role="dialog">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalTitle" style="font-size: 16pt; font font-weight: bold;">Informations</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p style="font-size: 16pt; text-align: center">Modification effectuée avec succès !</p>
        <img src="images/checked.svg" style="width: 10%; margin: 0 40%;">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
      </div>
    </div>
  </div>
</div>

<!-- LISTE DES TICKETS A DEFINIR -->
<div class="modal fade" id="message" role="dialog">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalTitle" style="font-size: 16pt; font font-weight: bold;">Des tickets en cours ne sont pas affectés</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <ul>
        <?php
          $requete = $bdd->query('SELECT * from tickets where technicien="A définir"');
          while($donnees = $requete->fetch()){
            echo "<li><a href='detailTicket.php?ticket=".$donnees['id']."'>Ticket n°".$donnees['id']." pour le client : ".$donnees['client']."</a></li>";
          }
        ?>
      </ul>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
      </div>
    </div>
  </div>
</div>

<!-- VALIDATION CLOTURE TICKET -->
<div class="modal fade" id="cloture" role="dialog">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Confirmation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Etes-vous sûr de vouloir clôturer ce ticket ?</p>
      </div>
      <div class="modal-footer">
        <?php
        echo "<a href='detailTicket.php?ticket=".$_GET['ticket']."&add=true&valider=cloturer'><button type='button' class='btn btn-danger'>Clôturer le ticket</button></a>";
        ?>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
      </div>
    </div>
  </div>
</div>

<!-- MODIFICATION DU MOT DE PASSE -->
<div class="modal fade" id="motDePasseNew" role="dialog">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Veuillez modifier votre mot de passe</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="home.php?filtre=current" method="POST">
        <div class="modal-body">           
          <br/><input type="password" name="password"  class="form-control" placeholder="Nouveau mot de passe" /><br/>    
        </div>
        <div class="modal-footer">
          <input type="submit" class="btn btn-primary" name="valider" value="Modifier"/>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- VALIDATION DESACTIVATION DE L'UILISATEUR -->
<div class="modal fade" id="desactivation" role="dialog">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Confirmation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p style="font-size: 16pt; text-align: center">L'utilisateur a été correctement désactivé</p>
        <img src="images/checked.svg" style="width: 10%; margin: 0 40%;">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
      </div>
    </div>
  </div>
</div>

<!-- VALIDATION ACTIVATION DE L'UILISATEUR -->
<div class="modal fade" id="activation" role="dialog">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Confirmation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p style="font-size: 16pt; text-align: center">L'utilisateur a été correctement activé</p>
        <img src="images/checked.svg" style="width: 10%; margin: 0 40%;">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
      </div>
    </div>
  </div>
</div>

<!-- REINITIALISATION DU MOT DE PASSE -->
<div class="modal fade" id="motDePasse" role="dialog">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Confirmation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Le mot de passe a été réinitialisé.</p>
        <p>Mot de passe = <b>password</b></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
      </div>
    </div>
  </div>
</div>